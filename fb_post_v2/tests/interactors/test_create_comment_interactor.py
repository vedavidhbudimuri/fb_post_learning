# Invalid PostId
# Valid Data
from unittest.mock import create_autospec

import pytest
from django_swagger_utils.drf_server.exceptions import NotFound

from fb_post_v2.interactors.create_comment_interactor import \
    CreateCommentInteractor
from fb_post_v2.interactors.presenters.presenter_interface import \
    PresenterInterface
from fb_post_v2.interactors.storages.storage_interface import StorageInterface


def test_with_invalid_post_id_raise_exception():
    # Arrange
    invalid_post_id = -1
    user_id = 1
    comment_content = "New Comment"

    storage = create_autospec(StorageInterface)
    presenter = create_autospec(PresenterInterface)

    storage.is_valid_post_id.return_value = False

    interactor = CreateCommentInteractor(
        storage=storage,
        presenter=presenter
    )
    presenter.raise_invalid_post_id_exception.side_effect = NotFound

    # Act
    with pytest.raises(NotFound):
        interactor.create_comment(
            post_id=invalid_post_id,
            user_id=user_id,
            comment_content=comment_content
        )

    # Assert
    presenter.raise_invalid_post_id_exception.assert_called_once()
