from unittest.mock import create_autospec

from fb_post_v2.interactors.create_post_interactor import CreatePostInteractor
from fb_post_v2.interactors.presenters.presenter_interface import \
    PresenterInterface
from fb_post_v2.interactors.storages.storage_interface import StorageInterface


def test_create_post():
    # Arrange
    user_id = 1
    post_content = "New Post"
    new_post_id = 1
    mock_presenter_response = {
        "post_id": new_post_id
    }
    storage = create_autospec(StorageInterface)
    presenter = create_autospec(PresenterInterface)
    storage.create_post.return_value = new_post_id
    presenter.get_create_post_response.return_value = mock_presenter_response

    interactor = CreatePostInteractor(
        storage=storage,
        presenter=presenter
    )

    # Act
    response = interactor.create_post(
        user_id=user_id, post_content=post_content
    )

    # Assert

    storage.create_post.assert_called_once_with(
        user_id=user_id, post_content=post_content
    )

    presenter.get_create_post_response.assert_called_once_with(
        post_id=new_post_id
    )
    assert response == 1
