from fb_post_v2.interactors.presenters.presenter_interface import \
    PresenterInterface
from fb_post_v2.interactors.storages.storage_interface import StorageInterface


class CreateCommentInteractor:

    def __init__(self, storage: StorageInterface,
                 presenter: PresenterInterface):
        self.storage = storage
        self.presenter = presenter

    def create_comment(self, user_id: int, post_id: int, comment_content: str):
        is_post_id_valid = self.storage.is_valid_post_id(post_id=post_id)
        is_invalid_post_id = not is_post_id_valid

        if is_invalid_post_id:
            self.presenter.raise_invalid_post_id_exception()
            return

        new_comment_id = self.storage.create_comment(
            user_id=user_id,
            post_id=post_id,
            comment_content=comment_content
        )
        response = self.presenter.get_create_comment_response(
            comment_id=new_comment_id
        )
        return response
