from abc import ABC
from abc import abstractmethod


class StorageInterface(ABC):

    @abstractmethod
    def create_post(self, user_id: int, post_content: str) -> int:
        pass

    @abstractmethod
    def is_valid_post_id(self, post_id: int) -> bool:
        pass

    @abstractmethod
    def raise_error_for_invalid_post_id(self, post_id: int):
        pass

    @abstractmethod
    def create_comment(self, user_id: int, post_id: int, comment_content: str) -> int:
        pass

