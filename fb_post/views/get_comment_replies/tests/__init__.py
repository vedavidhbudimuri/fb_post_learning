# pylint: disable=wrong-import-position

APP_NAME = "fb_post"
OPERATION_NAME = "get_comment_replies"
REQUEST_METHOD = "get"
URL_SUFFIX = "comment/{id}/reply/v1/"

from .test_case_01 import TestCase01GetCommentRepliesAPITestCase
from .test_case_02 import TestCase02GetCommentRepliesAPITestCase
from .test_case_03 import TestCase03GetCommentRepliesAPITestCase
from .test_case_04 import TestCase04GetCommentRepliesAPITestCase
from .test_case_05 import TestCase05GetCommentRepliesAPITestCase
from .test_case_06 import TestCase06GetCommentRepliesAPITestCase
from .test_case_07 import TestCase07GetCommentRepliesAPITestCase

__all__ = [
    "TestCase01GetCommentRepliesAPITestCase",
    "TestCase02GetCommentRepliesAPITestCase",
    "TestCase03GetCommentRepliesAPITestCase",
    "TestCase04GetCommentRepliesAPITestCase",
    "TestCase05GetCommentRepliesAPITestCase",
    "TestCase06GetCommentRepliesAPITestCase",
    "TestCase07GetCommentRepliesAPITestCase"
]
