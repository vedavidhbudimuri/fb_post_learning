# -*- coding: utf-8 -*-
# snapshottest: v1 - https://goo.gl/zC4yUc
from __future__ import unicode_literals

from snapshottest import Snapshot


snapshots = Snapshot()

snapshots['TestCase01GetCommentRepliesAPITestCase::test_case status'] = 200

snapshots['TestCase01GetCommentRepliesAPITestCase::test_case body'] = [
    {
        'comment_content': 'Reply to the Comment1 no0',
        'comment_id': 2,
        'commented_at': '2019-08-24 12:36:48.858351',
        'commenter': {
            'name': '',
            'profile_pic_url': '',
            'user_id': 1
        }
    },
    {
        'comment_content': 'Reply to the Comment1 no1',
        'comment_id': 3,
        'commented_at': '2019-08-24 12:36:48.858528',
        'commenter': {
            'name': '',
            'profile_pic_url': '',
            'user_id': 1
        }
    },
    {
        'comment_content': 'Reply to the Comment1 no2',
        'comment_id': 4,
        'commented_at': '2019-08-24 12:36:48.858696',
        'commenter': {
            'name': '',
            'profile_pic_url': '',
            'user_id': 1
        }
    },
    {
        'comment_content': 'Reply to the Comment1 no3',
        'comment_id': 5,
        'commented_at': '2019-08-24 12:36:48.858834',
        'commenter': {
            'name': '',
            'profile_pic_url': '',
            'user_id': 1
        }
    },
    {
        'comment_content': 'Reply to the Comment1 no4',
        'comment_id': 6,
        'commented_at': '2019-08-24 12:36:48.858972',
        'commenter': {
            'name': '',
            'profile_pic_url': '',
            'user_id': 1
        }
    }
]

snapshots['TestCase01GetCommentRepliesAPITestCase::test_case header_params'] = {
    'content-language': [
        'Content-Language',
        'en'
    ],
    'content-length': [
        '885',
        'Content-Length'
    ],
    'content-type': [
        'Content-Type',
        'text/html; charset=utf-8'
    ],
    'vary': [
        'Accept-Language, Origin, Cookie',
        'Vary'
    ],
    'x-frame-options': [
        'SAMEORIGIN',
        'X-Frame-Options'
    ]
}
