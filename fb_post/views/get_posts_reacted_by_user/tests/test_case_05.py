"""
# TODO: Update test case description
# DESCRIPTION: In this test case we are checking the test with negative offset input
                returns Exception
"""
import json

from django_swagger_utils.utils.test import CustomAPITestCase

from fb_post.constants.exception_messsages import get_random_reaction, OK
from fb_post.models import Post, Reaction, Comment
from fb_post.utils.test_utils.custom_test_util import TestFixtures
from . import APP_NAME, OPERATION_NAME, REQUEST_METHOD, URL_SUFFIX

REQUEST_BODY = """

"""

TEST_CASE = {
    "request": {
        "path_params": {},
        "query_params": {
            "offset": -9,
            "limit": 3
        },
        "header_params": {},
        "securities": {"oauth": {"tokenUrl": "http://auth.ibtspl.com/oauth2/", "flow": "password", "scopes": ["superuser"], "type": "oauth2"}},
        "body": REQUEST_BODY,
    },
}


class TestCase05GetPostsReactedByUserAPITestCase(TestFixtures):
    app_name = APP_NAME
    operation_name = OPERATION_NAME
    request_method = REQUEST_METHOD
    url_suffix = URL_SUFFIX
    test_case_dict = TEST_CASE

    def setupUser(self, username, password):
        super(TestCase05GetPostsReactedByUserAPITestCase, self).setupUser(
            username=username,
            password=password
        )
        self.create_post()
        self.create_post_reaction()

    def test_case(self):
        self.default_test_case()