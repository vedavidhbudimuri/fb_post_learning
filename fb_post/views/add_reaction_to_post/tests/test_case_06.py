"""
# TODO: Complete all three cases for this one
# DESCRIPTION: In this test case we are checking the test with valid input and
                reaction is inserting or not.

"""

from django_swagger_utils.utils.test import CustomAPITestCase

from fb_post.constants.exception_messsages import ACCEPTED
from fb_post.models import Post, Reaction
from fb_post.utils.test_utils.custom_test_util import TestFixtures
from . import APP_NAME, OPERATION_NAME, REQUEST_METHOD, URL_SUFFIX

REQUEST_BODY = """
{
    "reaction_type": "LIKE"
}
"""

TEST_CASE = {
    "request": {
        "path_params": {"id": 1},
        "query_params": {},
        "header_params": {},
        "securities": {
            "oauth": {"tokenUrl": "http://auth.ibtspl.com/oauth2/", "flow": "password",
                      "scopes": ["superuser"], "type": "oauth2"}},
        "body": REQUEST_BODY,
    },
}


class TestCase06AddReactionToPostAPITestCase(TestFixtures):
    app_name = APP_NAME
    operation_name = OPERATION_NAME
    request_method = REQUEST_METHOD
    url_suffix = URL_SUFFIX
    test_case_dict = TEST_CASE

    def setupUser(self, username, password):
        super(TestCase06AddReactionToPostAPITestCase, self).setupUser(
            username=username,
            password=password
        )
        Post.objects.create(user_id=self.foo_user.id, description="Nothing to post here.")

    def test_case(self):
        import json
        response = self.default_test_case()

        # reaction_type
        # reacted user
        # reacted post id

        reaction_type_from_response = json.loads(response.content)['reaction_type']
        reaction = Reaction.objects.get(post_id=1, user_id=self.foo_user.id)

        self.assert_match_snapshot(
            name='reacted_type',
            value=reaction.reaction_type
        )