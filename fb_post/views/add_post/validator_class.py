from django_swagger_utils.drf_server.exceptions import BadRequest
from django_swagger_utils.drf_server.utils.decorator.interface_decorator \
    import ValidatorAbstractClass

from fb_post.constants.exception_messsages import INVALID_POST_CONTENT


class ValidatorClass(ValidatorAbstractClass):
    def __init__(self, *args, **kwargs):
        self.request_data = kwargs['request_data']
        self.user = kwargs['user']
        self.user_dto = kwargs['user_dto']
        self.access_token = kwargs['access_token']

    def validate_post_content_length(self):

        post_content = self.request_data['post_content']
        if len(post_content) < 5:
            raise BadRequest(*INVALID_POST_CONTENT)

    def validate(self):
        """
        A wrapper function that calls all the validations and sends back
        necessary data.
        :return: A dictionary with values that are to be carry-forwarded to
        api_wrapper.
        """
        self.validate_post_content_length()
