"""
# TODO: Update test case description
# DESCRIPTION: In this test case we are checking the test with invalid post id

"""
import json

from django.db.models import Count
from django_swagger_utils.utils.test import CustomAPITestCase

from fb_post.constants.exception_messsages import NOT_FOUND, INVALID_POST_ID, \
    INVALID_COMMENT_ID, get_random_reaction, OK
from fb_post.models import Post, Reaction, Comment
from fb_post.utils.test_utils.custom_test_util import TestFixtures
from . import APP_NAME, OPERATION_NAME, REQUEST_METHOD, URL_SUFFIX

REQUEST_BODY = """
{
    "reaction_type": "LIKE"
}
"""

TEST_CASE = {
    "request": {
        "path_params": {"id": 1},
        "query_params": {},
        "header_params": {},
        "securities": {
            "oauth": {"tokenUrl": "http://auth.ibtspl.com/oauth2/", "flow": "password",
                      "scopes": ["superuser"], "type": "oauth2"}},
        "body": REQUEST_BODY,
    },
}


class TestCase03GetReactionMetricsAPITestCase(TestFixtures):
    app_name = APP_NAME
    operation_name = OPERATION_NAME
    request_method = REQUEST_METHOD
    url_suffix = URL_SUFFIX
    test_case_dict = TEST_CASE

    def setupUser(self, username, password):
        super(TestCase03GetReactionMetricsAPITestCase, self).setupUser(
            username=username,
            password=password
        )
        self.create_post_reaction()
        self.create_comment_reaction()

    def test_case(self):
        self.default_test_case()