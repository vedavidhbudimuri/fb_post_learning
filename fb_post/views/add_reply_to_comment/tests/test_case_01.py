"""
# TODO: Update test case description
# DESCRIPTION: In this test case we are checking the test with valid input
            * Success
                * Check Response Code
                * Check Reply Description
"""

from django_swagger_utils.utils.test import CustomAPITestCase

from fb_post.constants.exception_messsages import ACCEPTED
from fb_post.models import Post, Comment
from . import APP_NAME, OPERATION_NAME, REQUEST_METHOD, URL_SUFFIX

REQUEST_BODY = """
{
    "comment_description": "Reply to the comment"
}
"""

TEST_CASE = {
    "request": {
        "path_params": {"id": 1},
        "query_params": {},
        "header_params": {},
        "securities": {"oauth": {"tokenUrl": "http://auth.ibtspl.com/oauth2/", "flow": "password", "scopes": ["superuser"], "type": "oauth2"}},
        "body": REQUEST_BODY,
    },
}


class TestCase01AddReplyToCommentAPITestCase(CustomAPITestCase):
    app_name = APP_NAME
    operation_name = OPERATION_NAME
    request_method = REQUEST_METHOD
    url_suffix = URL_SUFFIX
    test_case_dict = TEST_CASE

    def setupUser(self, username, password):
        super(TestCase01AddReplyToCommentAPITestCase, self).setupUser(
            username=username,
            password=password
        )
        Post.objects.create(user_id=self.foo_user.id, description="Nothing to post here.")
        Comment.objects.create(user_id=self.foo_user.id,
                               post_id=1, description="Comment to post.")

    def test_case(self):
        response = self.default_test_case()
        response_code = response.status_code
        comment = Comment.objects.get(id=1)
        reply = comment.replies.all()[0]
        reply_description_api = "Reply to the comment"

        self.assertEqual(response_code, ACCEPTED)
        self.assertEqual(reply.description, reply_description_api)