from django_swagger_utils.drf_server.exceptions import NotFound
from django_swagger_utils.drf_server.utils.decorator.interface_decorator \
    import validate_decorator

from fb_post.constants.exception_messsages import INVALID_COMMENT_ID
from fb_post.utils.utils import reply_to_comment, InvalidCommentId
from .validator_class import ValidatorClass


@validate_decorator(validator_class=ValidatorClass)
def api_wrapper(*args, **kwargs):
    comment_id = kwargs['id']
    comment_description = kwargs['request_data']['comment_description']
    user_id = kwargs['user'].id

    import json
    from django.http.response import HttpResponse
    try:
        comment = reply_to_comment(comment_id,user_id, comment_description)
    except InvalidCommentId:
        raise NotFound(*INVALID_COMMENT_ID)

    response_data = json.dumps(
        {
            "comment_id": comment
        }
    )
    return HttpResponse(response_data, status=202)